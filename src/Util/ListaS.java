/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

/**
 *
 * @author profesor
 */
public class ListaS<T> {

    private Nodo<T> cabeza;
    private int size;

    public ListaS() {
        this.cabeza = null;
    }

    public void insertarInicio(T objeto) {
        Nodo<T> nuevo = new Nodo(objeto, this.cabeza);
        this.cabeza = nuevo;
        size++;
    }

    public boolean esVacio() {
        return this.cabeza == null;
    }

    public int getSize() {
        return size;
    }

    public String toString() {
        String msg = "cab<>";
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            msg += i.getInfo().toString() + "->";
        }
        return msg + "null";
    }

    public boolean contains(T objeto) {
        if (objeto == null || this.esVacio()) {
            throw new RuntimeException("no puede buscar un elemento");
        }
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            if (i.getInfo().equals(objeto)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        this.cabeza = null;
        this.size = 0;
    }

    private Nodo<T> getUltimo (){
        for (Nodo<T> i = cabeza; i != null ; i = i.getSig()) {
            if(i.getSig() == null){
                return i;
            }
        }
        return null;//simepre va a retonrnar algo 
    }

    public void insertarFinal(T objeto) {
        Nodo<T> nuevo = new Nodo(objeto, null);
        this.getUltimo().setSig(nuevo);
        size++;
    }
    
    public void virus(T info,int n){
        for (int i = 0; i < n ; i++) {
            insertarFinal(info);
        }
    }

}
