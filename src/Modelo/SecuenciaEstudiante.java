/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.Secuencia;

/**
 *
 * @author Docente
 */
public class SecuenciaEstudiante {

    private Secuencia<Estudiante> estudiantes;

    public SecuenciaEstudiante() {
    }

    public SecuenciaEstudiante(Estudiante[] est) {
        this.estudiantes = new Secuencia(est);
    }

    @Override
    public String toString() {

        String msg = "";
        for (int i = 0; i < this.estudiantes.size(); i++) {
            Estudiante estudiante = this.estudiantes.get(i);
            msg += estudiante.toString() + "\n";

        }
        return msg;
    }

    public void sort()
    {
        this.estudiantes.sort();
    }
}
